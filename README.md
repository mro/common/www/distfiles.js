# distfiles.js

Binary package repository for Node.js bindings.

Content of this repository is mirrored there: https://mro-dev.web.cern.ch/distfiles/js

This repository is used in combination with [distutils.js](https://gitlab.cern.ch/mro/common/www/distutils.js)

## CI configuration

To publish binary packages in this repository the following jobs should be created:
```yaml
include:
  - project: 'mro/common/tools/gitlab-ci-utils'
    file:
      # provides `.ci-js` and `.ci-artifacts-url` templates
      - 'ci/scripts/gitlab-ci-artifacts.yml'

ci-distfiles-prepare:
  image: node:lts-slim
  stage: build
  script:
    # example build recipe
    - apt-get update && apt-get install -y git python3 build-essential
    - npm install && npm run build:prod

    # preparing binary artifacts in "out"
    - dist-prepare out

    # Generate artifact_url.txt
    - !reference [.ci-artifacts-url, script]
  artifacts:
    paths:
      - out
      - artifact_url.txt

# By default this job will trigger only on version-tags
ci-distfiles:
  extends: .ci-js
  # get "out" and "artifact_url.txt" artifacts in this job
  needs: [ ci-distfiles-prepare ]
  variables:
    # This is the name of the job to trigger
    # it will be triggered with `ARTIFACT_URL` (the content of artifact_url.txt) as an argument
    job: mro/common/www/distfiles.js
```

### Authentication and private repositories

This repository must be able to fetch the job's artifacts using its own
`CI_JOB_TOKEN`, to do so special permissions must be granted in `Settings->CI/CD->Token Access`
on non-public repositories.